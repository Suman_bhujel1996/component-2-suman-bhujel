﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Graphical_Programming_Application;

namespace UnitTest1
{
    [TestClass]
    public class UnitTest1
    {
        
            [TestMethod]
            public void IsShapeEquals_Circle_ReturnTrue()
            {
                var shapeFactoryDef = new ShapeFactoryDefination();
                bool result = shapeFactoryDef.isCircle("circle");

                Assert.AreEqual(true, result);
            }
            [TestMethod]
            public void IsShapeEquals_Rectangle_ReturnTrue()
            {
                var shapeFactoryDef = new ShapeFactoryDefination();
                bool result = shapeFactoryDef.isRectangle("rectangle");

                Assert.AreEqual(true, result);
            }
            [TestMethod]
            public void IsShapeEquals_Square_ReturnTrue()
            {
                var shapeFactoryDef = new ShapeFactoryDefination();
                bool result = shapeFactoryDef.isSquare("square");

                Assert.AreEqual(true, result);
            }

            [TestMethod]
            public void IsShapeEquals_Triangle_ReturnTrue()
            {
                var shapeFactoryDef = new ShapeFactoryDefination();
                bool result = shapeFactoryDef.isTriangle("triangle");

                Assert.AreEqual(true, result);
            }
        }
    }

