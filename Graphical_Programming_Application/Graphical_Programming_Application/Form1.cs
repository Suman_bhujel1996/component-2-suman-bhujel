﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Graphical_Programming_Application
{
    public partial class Form1 : Form
    {
        Boolean saveChange;

        String filePath;

        Bitmap outputBitmap = new Bitmap(640, 480);
        public Form1()
        {
            InitializeComponent();
        }

        private void btnExecute_Click(object sender, EventArgs e)
        {

            string cmm = textBox1.Text.ToLower();
            string cod = richTextBox1.Text.ToLower();


            CodeParser cd = new CodeParser();
            string parsedCode = cd.Parser(cmm, cod);
            Graphics g = Graphics.FromImage(outputBitmap);
            g.DrawImageUnscaled(cd.GetBitmap(), 0, 0);
           flowLayoutPanel1.Refresh();
            if (parsedCode == "clear")
            {
                this.outputBitmap.Dispose();
                richTextBox1.Clear();
                Refresh();
            } 
            flowLayoutPanel1.Refresh();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
           

        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "Text Files|*.txt";

            if (sfd.ShowDialog() == DialogResult.OK)
            {
                System.IO.File.WriteAllText(sfd.FileName, richTextBox1.Text);
            }
        }

        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "Text Files|*.txt";

            if (ofd.ShowDialog() == DialogResult.OK)
            {
                richTextBox1.Text = System.IO.File.ReadAllText(ofd.FileName);
            }
        }

        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("For Your Help:\n" +
                         "draw circle 100\n" +
                         "draw rectangle 100 50\n" +
                         "draw triangle 10 10 100 10 50 60\n" +
                         "draw square 50\n" +
                         "move 100 100\n" +
                         "color red 23\n" +
                         "fill red\n" +
                         "fill no\n");
        }

        private void flowLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            g.DrawImageUnscaled(outputBitmap, 0, 0);
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void richTextBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {

                string cod = richTextBox1.Text.ToLower();


                CodeParser cm = new CodeParser();
                string parsedCode = cm.Parser("run", cod);
                if (parsedCode != null)
                {
                    MessageBox.Show(parsedCode);
                }
            }
        }
    }
}
