﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Graphical_Programming_Application
{
    public abstract class Shape : Shapes
    {
        protected Color colour; //shape's colour
        protected int x, y; //not I could use c# properties for this
        public Shape()
        {
            
            x = y = 100;
        }


        public Shape(Color colour, int x, int y)
        {

            this.colour = colour; //shape's colour
            this.x = x; //its x pos
            this.y = y; //its y pos
            //can't provide anything else as "shape" is too general
        }

        //the three methods below are from the Shapes interface
        //here we are passing on the obligation to implement them to the derived classes by declaring them as abstract
        public abstract void draw(Graphics g,Color c, int thickness); //any derrived class must implement this method
        public abstract double calcArea();
        public abstract double calcPerimeter();

        public virtual void setColor(Color colour)
        {
            this.colour = colour;

        }
        //set is declared as virtual so it can be overridden by a more specific child version
        //but is here so it can be called by that child version to do the generic stuff
        //note the use of the param keyword to provide a variable parameter list to cope with some shapes having more setup information than others
        public virtual void setX(int x)
        {
            
            this.x = x;
        }
        public virtual void setY(int y)
        {

            this.y = y;
        }


        public int getX()
        {
            return x;
        }
        public int getY()
        {
            return y;
        }

        public override string ToString()
        {
            return base.ToString() + "    " + this.x + "," + this.y + " : ";
        }
    }
}
