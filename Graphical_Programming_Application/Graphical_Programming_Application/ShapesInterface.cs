﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Threading.Tasks;

namespace Graphical_Programming_Application
{
    interface Shapes
    {
        void setX(int x);
        void setY(int y);
        void draw(Graphics g,Color c, int thickness);
        double calcArea();
        double calcPerimeter();
    }
}
