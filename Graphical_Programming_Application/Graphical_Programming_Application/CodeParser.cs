﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace Graphical_Programming_Application
{
    /// <summary>
    /// defining code parser class
    /// </summary>
    class CodeParser
    {
        Color c = Color.Black;
        Color fill = Color.Transparent;
        int moveX = 0, moveY = 0;
        int thickness = 3;


        int loopSize;
        Boolean loopFlag = false;
        List<string> loopContent;

        Boolean error = false;
        string error_cmm;
        Bitmap outputBitmap = new Bitmap(640, 480);
        Graphics g;

        int height = 0, width = 0, size = 0, radius = 0;
        public CodeParser()
        {
            outputBitmap = new Bitmap(640, 500);
            g = Graphics.FromImage(outputBitmap);
        }
        /// <summary>
        /// line and code for the shapes
        /// </summary>
        /// <param name="code"></param>
        /// <param name="line"></param>
        /// <returns></returns>
        public string checkCode(string code, int line)
        {
            int temp;

            char[] code_delimiters = new char[] { ' ' };
            String[] words = code.Split(code_delimiters, StringSplitOptions.RemoveEmptyEntries);

            if (words[0] == "draw")
            {
                if (words[1] == "circle")
                {
                    if (!(words.Length == 3))
                    {
                        error = true;
                        error_cmm = "The error in on line " + line + " please type correct code for 'draw circle'";
                        MessageBox.Show(error_cmm);
                        return error_cmm;
                    }
                    else if (Int32.TryParse(words[2], out temp))
                    {
                        Circle circle = new Circle();
                        circle.setColor(fill);
                        circle.setX(moveX);
                        circle.setY(moveY);
                        circle.setRadius(Convert.ToInt32(words[2]));
                        circle.draw(g, c, thickness);
                        //return null;
                    }
                    else if (words[2] == "radius")
                    {
                        Circle circle = new Circle();
                        circle.setColor(fill);
                        circle.setX(moveX);
                        circle.setY(moveY);
                        circle.setRadius(this.radius);
                        circle.draw(g, c, thickness);

                    }
                    else
                    {
                        MessageBox.Show("Please enter The parameters in integers in line " + line);
                        return "Please enter The parameters in integers in line " + line;
                    }
                }
                else if (words[1] == "square")
                {
                    if (!(words.Length == 3))
                    {
                        error = true;
                        error_cmm = "The error in on line " + line + " Please type correct code for 'draw square'";
                        return error_cmm;
                    }
                    else if (Int32.TryParse(words[2], out temp))
                    {
                        Square square = new Square();
                        square.setColor(fill);
                        square.setX(moveX);
                        square.setY(moveY);
                        square.setSize(Convert.ToInt32(words[2]));
                        square.draw(g, c, thickness);
                        //return null;
                    }
                    else if (words[2] == "size")
                    {
                        Square square = new Square();
                        square.setColor(fill);
                        square.setX(moveX);
                        square.setY(moveY);
                        square.setSize(this.size);
                        square.draw(g, c, thickness);
                    }
                    else
                    {
                        return "Please enter The parameters in integers in line " + line;

                    }
                }

                else if (words[1] == "rectangle")
                {
                    if (!(words.Length == 4))
                    {
                        error = true;
                        error_cmm = "The error in on line " + line + " Please type correct code for 'draw rectangle'";

                    }
                    else if (Int32.TryParse(words[2], out temp) && Int32.TryParse(words[3], out temp))
                    {
                        Rectangle rectangle = new Rectangle();
                        rectangle.setColor(fill);
                        rectangle.setX(moveX);
                        rectangle.setY(moveY);
                        rectangle.setHeight(Convert.ToInt32(words[2]));
                        rectangle.setWidth(Convert.ToInt32(words[3]));
                        rectangle.draw(g, c, thickness);
                        //return null;
                    }
                    else if ((words[2] == "height" || words[2] == "width") && (words[3] == "height" || words[3] == "width"))
                    {
                        Rectangle rectangle = new Rectangle();
                        rectangle.setColor(fill);
                        rectangle.setX(moveX);
                        rectangle.setY(moveY);
                        rectangle.setHeight(height);
                        rectangle.setWidth(width);
                        rectangle.draw(g, c, thickness);
                    }
                    else if (words[2] == "height" && words[3] != "width")
                    {
                        Rectangle rectangle = new Rectangle();
                        rectangle.setColor(fill);
                        rectangle.setX(moveX);
                        rectangle.setY(moveY);
                        rectangle.setHeight(height);
                        rectangle.setWidth(Convert.ToInt32(words[3]));
                        rectangle.draw(g, c, thickness);
                    }
                    else if (words[3] == "height" && words[2] != "width")
                    {
                        Rectangle rectangle = new Rectangle();
                        rectangle.setColor(fill);
                        rectangle.setX(moveX);
                        rectangle.setY(moveY);
                        rectangle.setHeight(height);
                        rectangle.setWidth(Convert.ToInt32(words[2]));
                        rectangle.draw(g, c, thickness);
                    }
                    else if (words[2] == "width" && words[3] != "height")
                    {
                        Rectangle rectangle = new Rectangle();
                        rectangle.setColor(fill);
                        rectangle.setX(moveX);
                        rectangle.setY(moveY);
                        rectangle.setHeight(width);
                        rectangle.setWidth(Convert.ToInt32(words[3]));
                        rectangle.draw(g, c, thickness);
                    }
                    else
                    {
                        return "Please enter The parameters in integers in line " + line;

                    }
                }
                else if (words[1] == "triangle")
                {
                    if (!(words.Length == 8))
                    {
                        error = true;
                        error_cmm = "The error in on line " + line + " Please type correct code for 'draw triangle'";

                    }
                    else if (Int32.TryParse(words[2], out temp) && Int32.TryParse(words[3], out temp) && Int32.TryParse(words[4], out temp) && Int32.TryParse(words[5], out temp) && Int32.TryParse(words[6], out temp) && Int32.TryParse(words[7], out temp))
                    {
                        Triangle triangle = new Triangle();
                        triangle.setColor(fill);
                        triangle.setPoint(Convert.ToInt32(words[2]), Convert.ToInt32(words[3]), Convert.ToInt32(words[4]), Convert.ToInt32(words[5]), Convert.ToInt32(words[6]), Convert.ToInt32(words[7]));
                        triangle.draw(g, c, thickness);
                        //return null;
                    }
                    else
                    {
                        return "Please enter The parameters in integers in line " + line;

                    }
                }
                else
                {
                    return "Please enter correct code in line " + line;
                }
            }
            else if (words[0] == "move")
            {
                if (!(words.Length == 3))
                {
                    error = true;
                    error_cmm = "The error in on line " + line + " Please type correct code for 'move'";
                    return error_cmm;
                }
                else if (Int32.TryParse(words[1], out temp) && Int32.TryParse(words[2], out temp))
                {
                    this.moveX = Convert.ToInt32(words[1]);
                    this.moveY = Convert.ToInt32(words[2]);
                    //return "Drawing Position changed to " + words[1] + " and " + words[2];
                }
                else
                {
                    return "Please enter The parameters in integers in line " + line;

                }
            }
           
            else if (words[0] == "color")
            {
                if (!(words.Length == 3))
                {
                    error = true;
                    error_cmm = "The error in on line " + line + " Please type correct code for 'move'";
                    return error_cmm;
                }
                else if (Int32.TryParse(words[2], out temp))
                {
                    this.thickness = Convert.ToInt32(words[2]);
                    Color cs = Color.FromName(words[1]);

                    if (cs.IsKnownColor)
                    {
                        this.c = Color.FromName(char.ToUpper(words[1][0]) + words[1].Substring(1));

                    }
                    else
                    {
                        return ("please enter a valid color");
                    }
                }
                else
                {
                    return "Please enter The parameters in integers" + line;

                }
            }

            else if (words[0] == "radius")
            {
                if (words[1] == "=" && Int32.TryParse(words[2], out temp))
                {
                    this.radius = Convert.ToInt32(words[2]);
                }
                else if (words[1] == "+=")
                {
                    this.radius = radius + Convert.ToInt32(words[2]);
                }

                else
                {
                    return "Please enter Correct Parameters";
                }
            }
            else if (words[0] == "height")
            {
                if (words[1] == "=" && Int32.TryParse(words[2], out temp))
                {
                    this.height = Convert.ToInt32(words[2]);
                }
                else if (words[1] == "+=")
                {
                    this.height = height + Convert.ToInt32(words[2]);
                }
                else
                {
                    return "Please enter Correct Parameters";
                }

            }
            else if (words[0] == "width")
            {
                if (words[1] == "=" && Int32.TryParse(words[2], out temp))
                {
                    this.width = Convert.ToInt32(words[2]);
                }
                else if (words[1] == "+=")
                {
                    this.width = width + Convert.ToInt32(words[2]);
                }
                else
                {
                    return "Please enter Correct Parameters";
                }

            }
            else if (words[0] == "size")
            {
                if (words[1] == "=" && Int32.TryParse(words[2], out temp))
                {
                    this.size = Convert.ToInt32(words[2]);
                }
                else if (words[1] == "+=")
                {
                    this.size = size + Convert.ToInt32(words[2]);
                }
                else
                {
                    return "Please enter Correct Parameters";
                }

            }
            else if (words[0] == "fill")
            {
                if (!(words.Length == 2))
                {
                    error = true;
                    error_cmm = "The error in on line " + line + " Please type correct code for 'move'";
                    return error_cmm;
                }

                else
                {
                    Color fil = Color.FromName(words[1]);
                    if (fil.IsKnownColor)
                    {
                        this.fill = Color.FromName(char.ToUpper(words[1][0]) + words[1].Substring(1));
                    }
                    else if (words[1] == "no")
                    {
                        this.fill = Color.Transparent;

                    }
                    else
                    {
                        return ("please enter a valid color");
                    }
                }
            }
            else if (words[0] == "loop")
            {
                this.loopFlag = true;
                this.loopSize = Convert.ToInt32(words[1]);
            }
            else if (words[0] == "endloop")
            {
                loopFlag = false;
                loopSize = 0;
            }
            else
            {
                return "Please enter The correct codes";
            }

            return null;
        }
        public string Parser(string comma, string code)
        {

            switch (comma)
            {
                case "run":
                    try
                    {
                        char[] delimiters = new char[] { '\r', '\n' };
                        string[] parts = code.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                        loopContent = new List<string>();
                        for (int i = 0; i < parts.Length; i++)
                        {
                            if (this.loopFlag == true)
                            {
                                loopContent.Add(parts[i]);
                            }
                            else
                            {

                                checkCode(parts[i], i + 1);
                            }


                        }
                        for (int k = 0; k < loopSize; k++)
                        {
                            for (int j = 0; j < loopContent.Count - 1; j++)
                            {
                                MessageBox.Show(loopContent[j]);
                                checkCode(loopContent[j], k);
                            }

                        }

                    }
                    catch (IndexOutOfRangeException ex)
                    {
                        return ex.Message;

                    }
                    catch (FormatException ex)
                    {
                        return "!!Please input correct parameter!!";
                    }
                    catch (ArgumentOutOfRangeException ex)
                    {
                        return "!!Please input correct parameter!!";
                    }
                    break;
                case "clear":
                    return "clear";
                case "reset":
                    this.moveX = 0;
                    this.moveY = 0;
                    this.thickness = 2;
                    this.fill = Color.Transparent;
                    c = Color.Black;
                    return "Pen reset to default value";
                case "help":
                    return "For Your Help:\n" +
                             "draw circle 100\n" +
                             "draw rectangle 100 50\n" +
                             "draw triangle 10 10 100 10 50 60\n" +
                             "draw square 50\n" +
                             "move 100 100\n" +
                             "color red 23\n" +
                             "fill red\n" +
                             "fill no\n";
                default:
                    return "Please enter a command";

            }

            return null;
        }
        /// <summary>
        /// returning bitmap
        /// </summary>
        /// <returns></returns>
        public Bitmap GetBitmap()
        {
            return this.outputBitmap;
        }
    }
}
