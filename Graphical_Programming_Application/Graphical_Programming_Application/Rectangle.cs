﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Graphical_Programming_Application
{
    /// <summary>
    ///  shape class for rectangle defined
    /// </summary>
    class Rectangle : Shape
    {
        /// <summary>
        /// declaration of variables
        /// </summary>
        int width, height;
        /// <summary>
        /// constructor creator
        /// </summary>
        public Rectangle() : base()
        {
            width = 100;
            height = 100;
        }
        public Rectangle(Color colour, int x, int y, int width, int height) : base(colour, x, y)
        {

            this.width = width; //the only thingthat is different from shape
            this.height = height;
        }

        public void setHeight(int height)
        {
            this.height = height;


        }
        /// <summary>
        /// rectangle width
        /// </summary>
        /// <param name="width"></param>
        public void setWidth(int width)
        {
            this.width = width;


        }
        /// <summary>
        /// thickness graphics and color of rectangle
        /// </summary>
        /// <param name="g"></param>
        /// <param name="c"></param>
        /// <param name="thickness"></param>
        public override void draw(Graphics g,Color c,int thickness)
        {
            Pen p = new Pen(c, thickness);
            g.DrawRectangle(p, x, y, width, height);
            SolidBrush b = new SolidBrush(colour);
            g.FillRectangle(b, x, y, width, height);
        }

        public override double calcArea()
        {
            return width * height;
        }

        public override double calcPerimeter()
        {
            return 2 * width + 2 * height;
        }
    }
}
