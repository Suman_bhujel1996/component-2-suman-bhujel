﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Graphical_Programming_Application
{
    /// <summary>
    /// shape class of square defined
    /// </summary>
    class Square: Rectangle
    {
        /// <summary>
        /// declaration of variable for square
        /// </summary>
        private int size;

        public Square() : base()
        {

        }
        /// <summary>
        /// setting up color and size of square
        /// </summary>
        /// <param name="colour"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="size"></param>
        public Square(Color colour, int x, int y, int size) : base(colour, x, y, size, size)
        {
            this.size = size;
        }

        public void setSize(int size)
        {
            this.size = size;
        }

        //no draw method here because it is provided by the parent class Rectangle
        public override void draw(Graphics g,Color c, int thickness)
        {
            base.draw(g,c,thickness);
        }
    }
}
